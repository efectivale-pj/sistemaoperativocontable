$(document).ready(
		function() {

			$("#repeat_pwd").change(
					function() {

						var pwdNew = "";
						var repeatNew = "";

						pwdNew = $("#new_pwd").val();
						repeatNew = $("#repeat_pwd").val();

						if (pwdNew != undefined && pwdNew != null
								&& repeatNew != undefined && repeatNew != null
								&& pwdNew.trim() != repeatNew.trim()) {
							mostrarError("Las contraseñas no coinciden");
							$("#repeat_pwd").val("");
						}

					});

			$("#aplicar").click(function() {

				// la funcion validaForm valida que los imputs no esten vacios
				var error = validaForm("Cambiopwd");
				if (error != "") {
					mostrarError(error);
				} else {
					// se obtiene la info de los inputs en tipo json
					var jsonRequest = getJsonForm("Cambiopwd");
					doCambioContrasenia(jsonRequest, this);
				}

			});

		});

function doCambioContrasenia(jsonRequest, objectElement, object) {

	// peticion ajax
	$.ajax({
		url : 'cambiaContrasenia',
		type : 'POST',
		dataType : 'json',
		data : {
			"ParametersRequest" : JSON.stringify(jsonRequest)
		// combierte cadena a json
		},
		beforeSend : function(xhr) {
			if (xhr && xhr.overrideMimeType) {
				xhr.overrideMimeType("application/j-son;charset=UTF-8");
			}
			console.log("Request Service...");
			$(objectElement).prop('disabled', true);

		},
		success : function(jsonResponse) {
			console.log(JSON.stringify(jsonResponse));
			if (jsonResponse._State == 'REDIRECT') {
				console.log("Error en la peticion:: " + jsonResponse._Message);
				mostrarError(jsonResponse._Message);
				window.location.href = '/SOC/log/out?mensaje='
						+ jsonResponse._Message;
			} else {
				// console.log("valida:: "+jsonResponse._State);
				if (jsonResponse._State == "true") {
					mostrarSuccess(jsonResponse._Message);

				} else {
					mostrarError(jsonResponse._Message);
				}
			}
		},
		error : function(jqXHR, exception) {
			console.log("[" + exception.statusText + "] ", exception);
			console.log("info: " + exception.statusText);
			alert(getErrorMessage(jqXHR, exception));
		},
		complete : function() {
			console.log("Completed [CambiaContrasenia]");
			$(objectElement).prop('disabled', false);

		}
	});

}
