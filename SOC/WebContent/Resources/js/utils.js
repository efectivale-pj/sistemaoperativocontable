
function validaCampo(tipo,objeto)
{
	console.log("Valida["+objeto.id+"] Tipo["+tipo+"] Value["+objeto.value+"]");
	switch(tipo)
	{
		case "INTEGER":
			var ExpresionRegularNumeros=/^\d*$/;
			if(objeto.value!=undefined && objeto.value!="" && !ExpresionRegularNumeros.test(objeto.value))
			{
				console.log("El campo solo recibe numeros. "+objeto.value);
				mostrarError("El campo solo recibe numeros. Por favor valida tu informacion e intentalo nuevamente.");
				objeto.value="";
				return false;
			}
		break;
		case "DOUBLE":
			var ExpresionRegularDouble=/^\d*\.\d*$/;
			if(objeto.value!=undefined && objeto.value!="" && !ExpresionRegularDouble.test(objeto.value))
			{
				console.log("El campo recibe numeros con punto decimal. "+objeto.value);
				mostrarError("El campo recibe numeros con punto decimal. Por favor valida tu informacion e intentalo nuevamente.");
				objeto.value="";
				return false;
			}
		break;
		case "STRING":
			var ExpresionRegularLetras=/^[a-zA-Z\s]*$/;
			if(objeto.value!=undefined && objeto.value!="" && !ExpresionRegularLetras.test(objeto.value))
			{
				console.log("El campo solo recibe letras, por favor omite caracteres especiales. "+objeto.value);
				mostrarError("El campo solo recibe letras, por favor omite caracteres especiales. Por favor valida tu informacion e intentalo nuevamente.");
				objeto.value="";
				return false;
			}
		break;
		case "EMAIL":
			var ExpresionRegularEmail=/^.[\w-_\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}$/;
			if(objeto.value!=undefined && objeto.value!="" && !ExpresionRegularEmail.test(objeto.value))
			{
				console.log("El correo capturado es incorrecto. "+objeto.value);
				mostrarError("El correo capturado es incorrecto. Por favor valida tu informacion e intentalo nuevamente.");
				objeto.value="";
				return false;
			}
		break;
		case "CURP":
			var ExpresionRegularCURP = /^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/;			
			if(objeto.value!=undefined && objeto.value!="")
			{
				if(!ExpresionRegularCURP.test(objeto.value.toUpperCase()))
				{
					console.log("CURP capturado es incorrecto. "+objeto.value);
					mostrarError("La CURP que se capturaro es incorrecto. Por favor valida tu informacion e intentalo nuevamente.");
					objeto.value="";
					return false;
				}
				else
					objeto.value=objeto.value.toUpperCase();
			}
		break;
		case "RFC":
			var ExpresionRegularRFC=/^([A-ZÑ\x26]{3}|[A-Z][AEIOU][A-Z]{2})\d{2}((01|03|05|07|08|10|12)(0[1-9]|[12]\d|3[01])|02(0[1-9]|[12]\d)|(04|06|09|11)(0[1-9]|[12]\d|30))[A-Z0-9]{2}[0-9A]$/;
			if(objeto.value!=undefined && objeto.value!="")
			{
				if(!ExpresionRegularRFC.test(objeto.value.toUpperCase()))
				{
					console.log("RFC capturado es incorrecto. "+objeto.value);
					mostrarError("El RFC que se capturaro es incorrecto. Por favor valida tu informacion e intentalo nuevamente.");
					objeto.value="";
					return false;
				}
				else
					objeto.value=objeto.value.toUpperCase();
			}
		break;
		case "CADENA":
			var ExpresionRegularLetras=/^[a-zA-Z0-9\s]*$/;
			if(objeto.value!=undefined && objeto.value!="" && !ExpresionRegularLetras.test(objeto.value))
			{
				console.log("El campo solo recibe letras, por favor omite caracteres especiales. "+objeto.value);
				mostrarError("El campo solo recibe letras, por favor omite caracteres especiales. Por favor valida tu informacion e intentalo nuevamente.");
				objeto.value="";
				return false;
			}
		break;
		case "NULO":
			if(objeto.value!=undefined && (objeto.value==" " || objeto.value.trim().length==0))
			{
				console.log("El campo esta vacio, por favor completa este campo. "+objeto.value);
				mostrarError("El campo esta vacio, por favor completa este campo. Por favor valida tu informacion e intentalo nuevamente.");
				objeto.value="";
				return false;
			}
		break;
		case "PASS":
			var logerror="";
			var expresionCompletaSimple=/^([A-Za-z\d]|[^ ]){4,15}$/;
			var lestrass=/^.*[A-Za-z].*$/;
			var numeross=/^.*\d.*$/;
			var espaciosblancoo=/^.* .*$/;
			var caracteresEspecialess=/^.*[$@!%*?&].*$/;
			if(!expresionCompletaSimple.test(objeto.value))
			{
				console.log("Password no supera las validaciones ["+objeto.value+"]");
				logerror="La contrasenia capturada no cumple con los requisitos minimos de complejidad.<br>";

				if(objeto.value.length<4 || objeto.value.length>15)
					logerror+=">Debe tener entre 4 y 15 caracteres de largo<br>";

				if(objeto.value.search(lestrass))
					logerror+=">Se recomienda incluir almenos una letra<br>";

				if(objeto.value.search(numeross))
					logerror+=">Se recomienda incluir almenos un numero<br>";

				if(objeto.value.search(caracteresEspecialess))
					logerror+=">Los caracteres especiales permitidos son [$@!%*?&]<br>";

				if(!objeto.value.search(espaciosblancoo))
					logerror+=">No es permitido incluir espacios en blanco<br>";
				logerror+="Por favor verifica tu contrasenia e ingresala nuevamente";
				console.log(logerror);
				mostrarError(logerror);
				objeto.value="";
				return false;
			}
		break;
		case "_PASS":
			var logerror="";
			var expresionCompleta=/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@!%*?&])([A-Za-z\d]|[^ ]){8,15}$/;
			var lestrasMayusculas=/^.*[A-Z].*$/;
			var lestrasMinisuculas=/^.*[a-z].*$/;
			var caracteresEspeciales=/^.*[$@!%*?&].*$/;
			var numeros=/^.*\d.*$/;
			var espaciosblanco=/^.* .*$/;
			if(!expresionCompleta.test(objeto.value))
			{
				console.log("Password no supera las validaciones ["+objeto.value+"]");
				logerror="La contrasenia capturada no cumple con los requisitos minimos de complejidad.<br>";

				if(objeto.value.length<7 || objeto.value.length>15)
					logerror+=">Debe tener entre 8 y 15 caracteres de largo<br>";

				if(objeto.value.search(lestrasMayusculas))
					logerror+=">Debe tener al menos una letra mayuscula<br>";

				if(objeto.value.search(lestrasMinisuculas))
					logerror+=">Debe tener al menos una letra minuscula<br>";

				if(objeto.value.search(numeros))
					logerror+=">Debe tener al menos un numero<br>";

				if(objeto.value.search(caracteresEspeciales))
					logerror+=">Debe tener al menos un caracter especial [$@!%*?&]<br>";

				if(!objeto.value.search(espaciosblanco))
					logerror+=">No es permitido incluir espacios en blanco<br>";
				logerror+="Por favor verifica tu contrasenia e ingresala nuevamente";
				console.log(logerror);
				mostrarError(logerror);
				objeto.value="";
				return false;
			}
		break;
		case "IMSS":
			
		break;
		case "INF":
			
		break;
		case "TEL":
			var ExpresionRegularTelefono=/^[0-9\s]{10}$/;
			if(objeto.value!=undefined && objeto.value!="" && !ExpresionRegularTelefono.test(objeto.value))
			{
				console.log("El numero de telefono debe contener solo 10 numeros. "+objeto.value);
				mostrarError("El numero de telefono o celular debe contener solo 10 numeros. Por favor valida tu informacion e intentalo nuevamente.");
				objeto.value="";
				return false;
			}
		break;
		case "CP":
			var ExpresionRegularCP=/^[0-9\s]{5}$/;
			if(objeto.value!=undefined && objeto.value!="" && !ExpresionRegularCP.test(objeto.value))
			{
				console.log("El coigo postal debe contener 5 numeros. "+objeto.value);
				mostrarError("El coigo postal debe contener 5 numeros. Por favor valida tu informacion e intentalo nuevamente.");
				objeto.value="";
				return false;
			}
		break;
		default:
			console.log("El tipo de dato no esta soportado para validacion["+tipo+"]");
			alert("El tipo de dato no esta soportado para validacion["+tipo+"]");
			return false;
	}
	return true;
}

function isInt(value) {
    var patt = new RegExp('^[^?\.?\s]*[0-9]$');
    var res = patt.test(value);
    return res;
}

function mostrarError(error)
{
//	swal({
//		  title: "Operacion Erronea!",
//		  text: error,
//		  icon: "error",
//		  dangerMode: true ,
//	});
	Swal.fire({
		  icon: 'error',
		  title: 'Operacion Erronea!',
		  html: error,
//		  footer: '<a href>Why do I have this issue?</a>'
		});
}

function mostrarInfo(info)
{
//	swal({
//		  title: "Info",
//		  text: info,
//		  icon: "info",
//		  dangerMode: true ,
//	});
	Swal.fire({
		  icon: 'info',
		  title: 'Info!',
		  text: info
//		  footer: '<a href>Why do I have this issue?</a>'
		});
}

function mostrarSuccess(success)
{
//	swal({
//		  title: "Operacion Exitosa!",
//		  text: success,
//		  icon: "success",
//		  dangerMode: true ,
//	});
	Swal.fire({
		  icon: 'success',
		  title: 'Operacion Exitosa!',
		  text: success
//		  footer: '<a href>Why do I have this issue?</a>'
		});
	
}
function validaForm(idForm) {
	var error = '';
	$("div[class='form-group "+idForm+"']").children().each(
			function() {

				if (this.type != undefined) {
					
					if (this.value == undefined || this.value == null
							|| String(this.value).trim().length < 1) {
						console.log("Value:: ["+this.value+"] id:: ["+this.id+"] type:: "+this.type);
						error += "<br> Es necesario completar "
								+ $(this).attr(
										"placeholder")+"\n";

					}

				}

			});
	return error;

}
//regresa un arreglo json
function getJsonForm(idForm) {
	var jsonForm = {};
	
	$("div[class='form-group "+idForm+"']").children().each(
			function() {

				if (this.type != undefined) {
					
					if (this.value != undefined || this.value != null
							|| String(this.value).trim().length > 0) {
//						console.log("Value:: ["+this.value+"] id:: ["+this.id+"] type:: "+this.type);

						jsonForm[this.id]=String(this.value).trim();
					}

				}

			});
	return jsonForm;

}