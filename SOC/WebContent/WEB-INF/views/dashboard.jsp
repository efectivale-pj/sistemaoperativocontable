<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<jsp:include page="includes/head.jsp" />
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> 
		</svg>
    </div>
	<div id="main-wrapper">
    	<jsp:include page="includes/menu-top.jsp"/>
		<jsp:include page="includes/menu.jsp" />
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">${permiso_active.procesos.procesonombre}</h3>
                    </div>
                  
                </div>
               <h1>Bienvenido!</h1>
            </div>
            <footer class="footer"> Auditorias Efectivale</footer>
        </div>
    </div>
	<jsp:include page="includes/footer.jsp" />
	
</body>


</html>