<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Estado de cuneta</title>
<jsp:include page="includes/head.jsp" />
</head>
<body class="fix-header fix-sidebar card-no-border">
<!-- Preloader - style you can find in spinners.css -->
	<!-- ============================================================== -->
	<div class="preloader">
		<svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none"
				stroke-width="2" stroke-miterlimit="10" /> 
		</svg>
	</div>

<div id="main-wrapper">
		<jsp:include page="includes/menu-top.jsp" />
		<jsp:include page="includes/menu.jsp" />
		<div class="page-wrapper">
			<div class="container-fluid">
				<div class="row page-titles">
					<div class="col-md-5 col-8 align-self-center">
						<h3 class="text-themecolor">${permiso_active.procesos.procesonombre}</h3>
					</div>
				</div>
				
				<h1>Estado de Cuenta!</h1>

			</div>

			<footer class="footer"> Auditorias Efectivale</footer>
		</div>
	</div>
<jsp:include page="includes/footer.jsp" />
<script
		src="<%=request.getContextPath()%>/resources/theme/assets/plugins/jquery/jquery-3.5.1.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/estadocuenta.js"></script>
</body>
</html>