<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

    <script src="<%=request.getContextPath()%>/resources/theme/assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<%=request.getContextPath()%>/resources/theme/assets/plugins/bootstrap/js/tether.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
   
    <script src="${pageContext.request.contextPath}/resources/theme/utils/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="${pageContext.request.contextPath}/resources/theme/utils/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="${pageContext.request.contextPath}/resources/theme/utils/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<%=request.getContextPath()%>/resources/theme/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="${pageContext.request.contextPath}/resources/theme/utils/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!-- chartist chart -->
    <script src="<%=request.getContextPath()%>/resources/theme/assets/plugins/chartist-js/dist/chartist.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/theme/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
    <!--c3 JavaScript -->
    <script src="<%=request.getContextPath()%>/resources/theme/assets/plugins/d3/d3.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/assets/plugins/c3-master/c3.min.js"></script>
    <!-- Chart JS -->
    <script src="${pageContext.request.contextPath}/resources/theme/utils/js/dashboard1.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/utils.js"></script>
        <script src="${pageContext.request.contextPath}/resources/alert/sweetalert2.all.min.js"></script>