<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<%=request.getContextPath()%>/resources/theme/assets/images/favicon.png">
    <title>Estado de cuenta - Efectivale</title>
    <!-- Bootstrap Core CSS -->
    <link href="<%=request.getContextPath()%>/resources/theme/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="<%=request.getContextPath()%>/resources/theme/assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="<%=request.getContextPath()%>/resources/theme/assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
    <link href="<%=request.getContextPath()%>/resources/theme/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <!--This page css - Morris CSS -->
    <link href="<%=request.getContextPath()%>/resources/theme/assets/plugins/c3-master/c3.min.css" rel="stylesheet">
    
    <!-- Custom CSS -->
    <link href="${pageContext.request.contextPath}/resources/theme/utils/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="${pageContext.request.contextPath}/resources/theme/utils/css/colors/reed.css" id="theme" rel="stylesheet">