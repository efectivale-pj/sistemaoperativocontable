<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Login</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/login.css?idRandom=${pageContext.session.id}">
	<!-- SweetAlert -->
	<script src="${pageContext.request.contextPath}/resources/alert/sweetalert2.all.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/utils.js?idRandom=${pageContext.session.id}""></script>
</head>
<body>
	<c:if test="${view_error_jsp.length()>0}">
		<script type="text/javascript">mostrarError('${view_error_jsp}');</script>
	</c:if>
	<c:if test="${view_mensaje_jsp.length()>0}">
		<script type="text/javascript">mostrarInfo("${view_mensaje_jsp}");</script>
	</c:if>
	<div id="containerMostrarMensajes"></div>
	<div class="container h-100">
		<div class="d-flex justify-content-center h-100">
			<div class="user_card">
				<div class="d-flex justify-content-center">
					<div class="brand_logo_container">
						<img src="${pageContext.request.contextPath}/resources/img/efv_icon.png" class="brand_logo" alt="Logo">
					</div>
				</div>
				<div class="d-flex justify-content-center form_container">
					<form:form action="loginProcess" modelAttribute="usuario" method="POST">
						<div class="input-group mb-3">
							<div class="input-group-append">
								<span class="input-group-text"><i class="fas fa-user"></i></span>
							</div>
							<form:input class="form-control form-control-user" path="usuariousr" placeholder="Usuario" required="true"/>
						</div>
						<div class="input-group mb-2">
							<div class="input-group-append">
								<span class="input-group-text"><i class="fas fa-key"></i></span>
							</div>
							<form:password class="form-control form-control-user" path="usuariopwd" placeholder="Password" required="true"/>
						</div>
						<div class="d-flex justify-content-center mt-3 login_container">
							<button type="submit" name="button" class="btn login_btn">Login</button>
						</div>
					</form:form>
				</div>
		
				<div class="mt-4">
					<div class="d-flex justify-content-center links">
						Don't have an account? <a href="#" class="ml-2" onclick="mostrarInfo('Comunicate con tu departamento de sistemas');">Sign Up</a>
					</div>
					<div class="d-flex justify-content-center links">
						<a href="#" onclick="mostrarInfo('Comunicate con tu departamento de sistemas');">Forgot your password?</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
