<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<jsp:include page="includes/head.jsp" />

</head>


<body class="fix-header fix-sidebar card-no-border">
	<!-- ============================================================== -->
	<!-- Preloader - style you can find in spinners.css -->
	<!-- ============================================================== -->
	<div class="preloader">
		<svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none"
				stroke-width="2" stroke-miterlimit="10" /> 
		</svg>
	</div>
	<div id="main-wrapper">
		<jsp:include page="includes/menu-top.jsp" />
		<jsp:include page="includes/menu.jsp" />
		<div class="page-wrapper">
			<div class="container-fluid">
				<div class="row page-titles">
					<div class="col-md-5 col-8 align-self-center">
				<h3 class="text-themecolor">${permiso_active.procesos.procesonombre}</h3>
					</div>
				</div>
				<h4>Fecha ultimo ingreso ${usuariosession.usuarioultimoacceso}</h4>

				<h2>Cambiar contraseņa</h2>

				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-6">
						<form id="form-cambiocontrasenia">
							
							<div class="form-group Cambiopwd">
								<label>Contraseņa actual</label> <input class="form-control"
									placeholder="contraseņa actual" id="old_pwd"
									type="password" required>
							</div>
							<div class="form-group Cambiopwd">
								<label>Nueva Contraseņa</label> <input class="form-control"
									placeholder="contraseņa nueva" id="new_pwd"
									type="password" onchange="validaCampo('PASS',this);" required>
							</div>
							<div class="form-group Cambiopwd">
								<label>Confirmar Nueva Contraseņa</label> <input
									class="form-control" placeholder="confirma contraseņa"
									id="repeat_pwd" type="password"
									onchange="validaCampo('PASS',this);" required>
							</div>
							<div style="text-align: center;">
								<input type="button" class="btn btn-success"
									id="aplicar" value="Guardar" />
							</div>
							<br>
							

							
						</form>
					</div>
					<div class="col-md-3"></div>
				</div>




			</div>

			<footer class="footer"> Auditorias Efectivale</footer>
		</div>
	</div>
	<jsp:include page="includes/footer.jsp" />

	<script
		src="<%=request.getContextPath()%>/resources/theme/assets/plugins/jquery/jquery-3.5.1.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/perfil.js"></script>
</body>
</html>