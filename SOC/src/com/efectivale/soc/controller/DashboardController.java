package com.efectivale.soc.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.efectivale.soc.dao.UsuariosDao;
import com.efectivale.soc.dao.impl.UsuariosDaoImpl;
import com.efectivale.soc.model.ApiServiceResponse;
import com.efectivale.soc.model.Bitacora;
import com.efectivale.soc.model.Usuarios;
import com.efectivale.soc.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

@Controller
@RequestMapping("/dashboard")
public class DashboardController {
	private Logger log = Logger.getLogger(DashboardController.class);
	private Utils utils = new Utils();

	@RequestMapping("/")
	public String view(HttpServletRequest request, Model theModel,
			@RequestParam(value = "view_mensaje_jsp", required = false) String view_mensaje_jsp,
			@RequestParam(value = "view_error_jsp", required = false) String view_error_jsp) {
		log.info("Inicia");
		Bitacora bitacora = utils.isSessionActive(request);
		System.out.println(bitacora);
		if (bitacora == null) {
			return "redirect:/log/out?mensaje=" + utils.getDetailError();
		}
		theModel.addAttribute("permiso_active", bitacora.getPermiso());
		theModel.addAttribute("view_mensaje_jsp", view_mensaje_jsp);
		theModel.addAttribute("view_error_jsp", view_error_jsp);

		return "dashboard";
	}

	@RequestMapping("/perfil/")
	public String perfil(HttpServletRequest request, Model theModel) {
		log.info("Inicia");
		Bitacora bitacora = utils.isSessionActive(request);
		if (bitacora == null) {
			return "redirect:/log/out?mensaje=" + utils.getDetailError();
		}
		theModel.addAttribute("permiso_active", bitacora.getPermiso());
		//

		return "perfil";
	}

	@RequestMapping("/perfil/cambiaContrasenia")
	public @ResponseBody String cambiaContrasenia(HttpServletRequest request, Model theModel,
			@RequestParam String ParametersRequest) {
		log.info("inicia cambio de contrase�a");
		log.info(ParametersRequest);
		JsonObject jsonObject = null;
		ApiServiceResponse response = new ApiServiceResponse();
		UsuariosDao usuarioDao = null;
		Usuarios user = null;

		try {
			if (!utils.sessionActive(request)) {
				response.set_State("REDIRECT");
				response.set_Message(utils.getDetailError());
				return new Gson().toJson(response);
			}
			jsonObject = new Gson().fromJson(ParametersRequest, JsonObject.class);
			usuarioDao = new UsuariosDaoImpl();
			user = (Usuarios) request.getSession().getAttribute("usuariosession");
			String usuariopwd_old = jsonObject.get("old_pwd").getAsString();
			String usuariopwd_new = jsonObject.get("new_pwd").getAsString();
			log.info(user.toString());
			log.info("contrase�a actual " + usuariopwd_old);
			log.info("contrase�a nueva " + usuariopwd_new);
			log.info("contrase�a confirmada " + jsonObject.get("repeat_pwd").getAsString());
			if (usuarioDao.CambiaContrasenia(user, usuariopwd_old, usuariopwd_new)) {
				response.set_State("true");
				response.set_Message("Contrasenia actualizada");

			} else {

				response.set_State("false");
				response.set_Message(usuarioDao.getDetailError());

			}
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error("Error al procesar la peticion", ex);
			response = new ApiServiceResponse();
			response.set_State("false");
			response.set_Message("No se logro procesar tu peticion:: " + ex.getMessage());
		}
		return new Gson().toJson(response);
	}

}
