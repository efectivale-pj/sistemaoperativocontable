package com.efectivale.soc.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.efectivale.soc.model.Bitacora;
import com.efectivale.soc.utils.Utils;

@Controller
@RequestMapping("/estadocuenta")
public class EstadoCuentaController {
	private Logger log = Logger.getLogger(EstadoCuentaController.class);
	private Utils utils = new Utils();

	@RequestMapping("/")
	public String viewAccount(HttpServletRequest request, Model theModel) {
		log.info("Inicia");
		Bitacora bitacora = utils.isSessionActive(request);
		System.out.println(bitacora);
		if (bitacora == null) {
			return "redirect:/log/out?mensaje=" + utils.getDetailError();
		}
		theModel.addAttribute("permiso_active", bitacora.getPermiso());
		return "estadocuenta";
	}

}
