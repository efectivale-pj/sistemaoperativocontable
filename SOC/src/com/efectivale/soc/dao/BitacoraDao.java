package com.efectivale.soc.dao;

import com.efectivale.soc.model.Bitacora;

public interface BitacoraDao {

	public String getDetailError();

	public boolean createBitacora(Bitacora bitacora);

	public boolean updateBitacora(Bitacora bitacora);

}
