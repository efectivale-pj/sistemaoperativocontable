package com.efectivale.soc.dao;

import com.efectivale.soc.model.Usuarios;

public interface UsuariosDao {

	public String getDetailError();
	
	public boolean updateUsuario(Usuarios usuario);
	
	public boolean CambiaContrasenia(Usuarios usuario, String usuariopwd_old, String usuariopwd_new);

	public Usuarios validateUsuario(Usuarios usuario, int numeroIntentos);

}
