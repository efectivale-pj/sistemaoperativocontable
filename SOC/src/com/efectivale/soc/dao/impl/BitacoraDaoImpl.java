package com.efectivale.soc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import com.efectivale.soc.dao.BitacoraDao;
import com.efectivale.soc.model.Bitacora;
import com.efectivale.soc.utils.Scripts;
import com.efectivale.soc.utils.Utils;

public class BitacoraDaoImpl implements BitacoraDao {
	private Logger log = Logger.getLogger(BitacoraDaoImpl.class);
	private Utils utils=new Utils();
	private String DetailError;
	
	@Override
	public String getDetailError() {
		return DetailError;
	}

	@Override
	public boolean updateBitacora(Bitacora bitacora) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		try
		{
			connection = utils.getConnection("dbsoc");
			connection.setAutoCommit(true);
			preparedStatement = connection.prepareStatement(Scripts.SPUpdateBitacora);
			preparedStatement.setInt(1, bitacora.getProcesoid());
			preparedStatement.setInt(2, bitacora.getUsuarioid());
			preparedStatement.setString(3, bitacora.getBitacorapeticion());
			preparedStatement.setString(4, bitacora.getBitacorarespuesta());
			preparedStatement.setString(5, bitacora.getBitacoramensaje());
			preparedStatement.setString(6, bitacora.getBitacoraerror());
			preparedStatement.setString(7, bitacora.getBitacoraip());
			preparedStatement.setTimestamp(8, bitacora.getBitacorafecha());
			preparedStatement.setInt(9, bitacora.getBitacoraid());
			if((resultSet = preparedStatement.executeQuery()).next())
			{
//				log.info("ResponseSP:: "+resultSet.getString("rs_details"));
				if(resultSet.getBoolean("rs_status"))
					return true;
				else
					DetailError=resultSet.getString("rs_details");
			}
			else
			{
				log.error("No se recibio respuesta del SP:: "+preparedStatement.toString());
				DetailError="No se recibio respuesta del sp_create_bitacora";
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al actualizar en bitacor",ex);
			DetailError="Se genero un error al actualizar en bitacora, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return false;
	}

	@Override
	public boolean createBitacora(Bitacora bitacora) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		try
		{
			connection = utils.getConnection("dbsoc");
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(Scripts.SPcreateBitacora);
			preparedStatement.setInt(1, bitacora.getProcesoid());
			preparedStatement.setInt(2, bitacora.getUsuarioid());
			preparedStatement.setString(3, bitacora.getBitacorapeticion());
			preparedStatement.setString(4, bitacora.getBitacorarespuesta());
			preparedStatement.setString(5, bitacora.getBitacoramensaje());
			preparedStatement.setString(6, bitacora.getBitacoraerror());
			preparedStatement.setString(7, bitacora.getBitacoraip());
			preparedStatement.setTimestamp(8, bitacora.getBitacorafecha());
			if((resultSet = preparedStatement.executeQuery()).next())
			{
//				log.info("ResponseSP:: "+resultSet.getString("rs_details"));
				if(resultSet.getBoolean("rs_status"))
				{
					connection.commit();
					bitacora.setBitacoraid(resultSet.getInt("rs_id"));
					return true;
				}
				else
					DetailError=resultSet.getString("rs_details");
			}
			else
			{
				log.error("No se recibio respuesta del SP:: "+preparedStatement.toString());
				DetailError="No se recibio respuesta del sp_create_bitacora";
			}
			connection.rollback();
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al registrar en bitacor",ex);
			DetailError="Se genero un error al registrar en bitacora, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return false;
	}

}
