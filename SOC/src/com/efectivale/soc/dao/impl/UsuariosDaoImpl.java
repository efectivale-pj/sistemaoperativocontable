package com.efectivale.soc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.efectivale.soc.dao.UsuariosDao;
import com.efectivale.soc.model.Permisos;
import com.efectivale.soc.model.Procesos;
import com.efectivale.soc.model.Usuarios;
import com.efectivale.soc.utils.Scripts;
import com.efectivale.soc.utils.Utils;

public class UsuariosDaoImpl implements UsuariosDao {
	private Logger log = Logger.getLogger(UsuariosDaoImpl.class);
	private Utils utils=new Utils();
	private String DetailError;
	
	@Override
	public String getDetailError() {
		return DetailError;
	}
	public void setDetailError(String detailError) {
		this.DetailError = detailError;
	}

	@Override
	public boolean updateUsuario(Usuarios usuario) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		try
		{
			connection = utils.getConnection("dbsoc");
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(Scripts.SPUpdateUsuario);
			preparedStatement.setInt(1, usuario.getUsuarioid());
			preparedStatement.setInt(2, usuario.getPerfilid());
			preparedStatement.setBoolean(3, usuario.isUsuarioactivo());
			preparedStatement.setString(4, usuario.getUsuariousr());
			preparedStatement.setString(5, usuario.getUsuariopwd());
			preparedStatement.setString(6, usuario.getUsuarionombre());
			preparedStatement.setTimestamp(9, usuario.getUsuariofechacreacion());
			preparedStatement.setTimestamp(10, usuario.getUsuariofechamodificacion());
			preparedStatement.setTimestamp(11, usuario.getUsuarioultimoacceso());
			preparedStatement.setString(12, usuario.getUsuariokey());
			log.info(preparedStatement.toString());
			if((resultSet = preparedStatement.executeQuery()).next()) 
			{
				log.info("Responses SP:: "+resultSet.getString("rs_details"));
				if(resultSet.getBoolean("rs_status"))
				{
					connection.commit();
					return true;
				}
				else
					DetailError=resultSet.getString("rs_details");
			}
			else
			{
				log.error("No se recibio respuesta del sp:: "+preparedStatement.toString());
				DetailError="No se recibio respuesta";
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al actualizar el usuario: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return false;
	}

	@Override
	public boolean CambiaContrasenia(Usuarios usuario, String usuariopwd_old, String usuariopwd_new) {
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		try
		{
			connection = utils.getConnection("dbsoc");
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(Scripts.SPCambiarContrasenia);
			preparedStatement.setInt(1, usuario.getUsuarioid());
			preparedStatement.setString(2, usuariopwd_old);
			preparedStatement.setString(3, usuariopwd_new);

			if((resultSet = preparedStatement.executeQuery()).next()) 
			{
				log.info("Responses SP:: "+resultSet.getString("rs_details"));
				log.info(preparedStatement.toString());
				if(resultSet.getBoolean("rs_status"))
				{
					usuario.setUsuariopwd(resultSet.getString("rs_usuariopwd"));
					connection.commit();
					return true;
				}
				else
					DetailError=resultSet.getString("rs_details");
			}
			else
			{
				log.error("No se recibio respuesta del sp:: "+preparedStatement.toString());
				DetailError="No se recibio respuesta";
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al cambiar la contrasenia: "+ex.getMessage(),ex);
			DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return false;
	}

	@Override
	public Usuarios validateUsuario(Usuarios usuario, int intentos) {
		log.info("Valida usuario ["+usuario.getUsuariousr()+"]");
		Connection connection = null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet = null;
		try
		{
			connection = utils.getConnection("dbsoc");
			connection.setAutoCommit(true);
			preparedStatement = connection.prepareStatement(Scripts.SPvalidateUsuario);
			preparedStatement.setString(1, usuario.getUsuariousr());
			preparedStatement.setString(2, usuario.getUsuariopwd());
			preparedStatement.setInt(3, intentos);
			if((resultSet = preparedStatement.executeQuery()).next()) 
			{
				log.info("Responses SP:: "+resultSet.getString("rs_details"));
				if(resultSet.getBoolean("rs_status"))
				{
					usuario.setUsuarioid(resultSet.getInt("rs_id"));
					utils.closeConnection(null, preparedStatement, resultSet);
					
					preparedStatement = connection.prepareStatement(Scripts.ViewUsuarioById);
					preparedStatement.setInt(1, usuario.getUsuarioid());
					if((resultSet = preparedStatement.executeQuery()).next())
					{
						usuario = utils.getObjectUsuario(resultSet);
						usuario.setPerfil(utils.getObjectPerfil(resultSet));
						usuario.getPerfil().setPermisos(new ArrayList<Permisos>());
						do {
							Permisos permiso = utils.getObjectPermisos(resultSet);
							Procesos proceso = utils.getObjectProcesos(resultSet);
							permiso.setProcesos(proceso);
							usuario.getPerfil().getPermisos().add(permiso);
						}while(resultSet.next());
						return usuario;
					}
					else {
						log.error("No se encontro informacion del usuarioid:: "+preparedStatement.toString());
						DetailError="No se encontro informacion del usuario";
					}
				}
				else
					DetailError=resultSet.getString("rs_details");
			}
			else
			{
				log.error("No se recibio respuesta del sp:: "+preparedStatement.toString());
				DetailError="No se recibio respuesta";
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Se genero un error al validar el usuario: "+ex.getMessage(),ex);
			if(ex.getMessage().indexOf("P0001")>=0 && ex.getMessage().indexOf("Contrasenia incorrecta")>=0)
			{
				DetailError="Contraseña incorrecta";
			}
			else
				DetailError="Se genero un error, por favor intentelo mas tarde. "+ex.getMessage();
		}
		finally
		{
			utils.closeConnection(connection, preparedStatement, resultSet);
		}
		return null;
	}
}
