package com.efectivale.soc.model;

import java.sql.Timestamp;

public class Bitacora {
	private int bitacoraid;
	private int procesoid;
	private int usuarioid;
	private String bitacorapeticion;
	private String bitacorarespuesta;
	private String bitacoramensaje;
	private String bitacoraerror;
	private String bitacoraip;
	private Timestamp bitacorafecha;
	private Permisos permiso;
	public int getBitacoraid() {
		return bitacoraid;
	}
	public void setBitacoraid(int bitacoraid) {
		this.bitacoraid = bitacoraid;
	}
	public int getProcesoid() {
		return procesoid;
	}
	public void setProcesoid(int procesoid) {
		this.procesoid = procesoid;
	}
	public int getUsuarioid() {
		return usuarioid;
	}
	public void setUsuarioid(int usuarioid) {
		this.usuarioid = usuarioid;
	}
	public String getBitacorapeticion() {
		return bitacorapeticion;
	}
	public void setBitacorapeticion(String bitacorapeticion) {
		this.bitacorapeticion = bitacorapeticion;
	}
	public String getBitacorarespuesta() {
		return bitacorarespuesta;
	}
	public void setBitacorarespuesta(String bitacorarespuesta) {
		this.bitacorarespuesta = bitacorarespuesta;
	}
	public String getBitacoramensaje() {
		return bitacoramensaje;
	}
	public void setBitacoramensaje(String bitacoramensaje) {
		this.bitacoramensaje = bitacoramensaje;
	}
	public String getBitacoraerror() {
		return bitacoraerror;
	}
	public void setBitacoraerror(String bitacoraerror) {
		this.bitacoraerror = bitacoraerror;
	}
	public String getBitacoraip() {
		return bitacoraip;
	}
	public void setBitacoraip(String bitacoraip) {
		this.bitacoraip = bitacoraip;
	}
	public Timestamp getBitacorafecha() {
		return bitacorafecha;
	}
	public void setBitacorafecha(Timestamp bitacorafecha) {
		this.bitacorafecha = bitacorafecha;
	}
	public Permisos getPermiso() {
		return permiso;
	}
	public void setPermiso(Permisos permiso) {
		this.permiso = permiso;
	}
	@Override
	public String toString() {
		return "Bitacora [bitacoraid=" + bitacoraid + ", procesoid=" + procesoid
				+ ", usuarioid=" + usuarioid + ", bitacorapeticion=" + bitacorapeticion + ", bitacorarespuesta="
				+ bitacorarespuesta + ", bitacoramensaje=" + bitacoramensaje + ", bitacoraerror=" + bitacoraerror
				+ ", bitacoraip=" + bitacoraip + ", bitacorafecha=" + bitacorafecha + ", permiso=" + permiso + "]";
	}
}
