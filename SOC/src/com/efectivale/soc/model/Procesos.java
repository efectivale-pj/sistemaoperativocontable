package com.efectivale.soc.model;

import java.io.Serializable;
import java.sql.Timestamp;

public class Procesos implements Serializable {
	private static final long serialVersionUID = 4561822448808686798L;
	private int procesoid;
	private boolean procesoactivo;
	private String procesonombre;
	private String procesodescripcion;
	private Timestamp procesofechacreacion;
	private Timestamp procesofechamodificacion;
	private String procesopath;
	private int procesopadreid;
	private boolean procesoprincipal;
	private int procesoorden;
	public int getProcesoid() {
		return procesoid;
	}
	public void setProcesoid(int procesoid) {
		this.procesoid = procesoid;
	}
	public boolean isProcesoactivo() {
		return procesoactivo;
	}
	public void setProcesoactivo(boolean procesoactivo) {
		this.procesoactivo = procesoactivo;
	}
	public String getProcesonombre() {
		return procesonombre;
	}
	public void setProcesonombre(String procesonombre) {
		this.procesonombre = procesonombre;
	}
	public String getProcesodescripcion() {
		return procesodescripcion;
	}
	public void setProcesodescripcion(String procesodescripcion) {
		this.procesodescripcion = procesodescripcion;
	}
	public Timestamp getProcesofechacreacion() {
		return procesofechacreacion;
	}
	public void setProcesofechacreacion(Timestamp procesofechacreacion) {
		this.procesofechacreacion = procesofechacreacion;
	}
	public Timestamp getProcesofechamodificacion() {
		return procesofechamodificacion;
	}
	public void setProcesofechamodificacion(Timestamp procesofechamodificacion) {
		this.procesofechamodificacion = procesofechamodificacion;
	}
	public String getProcesopath() {
		return procesopath;
	}
	public void setProcesopath(String procesopath) {
		this.procesopath = procesopath;
	}
	public int getProcesopadreid() {
		return procesopadreid;
	}
	public void setProcesopadreid(int procesopadreid) {
		this.procesopadreid = procesopadreid;
	}
	public boolean isProcesoprincipal() {
		return procesoprincipal;
	}
	public void setProcesoprincipal(boolean procesoprincipal) {
		this.procesoprincipal = procesoprincipal;
	}
	public int getProcesoorden() {
		return procesoorden;
	}
	public void setProcesoorden(int procesoorden) {
		this.procesoorden = procesoorden;
	}
	@Override
	public String toString() {
		return "Procesos [procesoid=" + procesoid + ", procesoactivo="
				+ procesoactivo + ", procesonombre=" + procesonombre + ", procesodescripcion=" + procesodescripcion
				+ ", procesofechacreacion=" + procesofechacreacion + ", procesofechamodificacion="
				+ procesofechamodificacion + ", procesopath=" + procesopath + ", procesopadreid=" + procesopadreid
				+ ", procesoprincipal=" + procesoprincipal + ", procesoorden=" + procesoorden + "]";
	}
}
