package com.efectivale.soc.model;

import java.io.Serializable;
import java.sql.Timestamp;

public class Usuarios implements Serializable {
	private static final long serialVersionUID = 6546972622622975139L;
	private int usuarioid;
	private int perfilid;
	private int empleadoid;
	private boolean usuarioactivo;
	private String usuarionombre;
	private String usuariousr;
	private String usuariopwd;
	private Timestamp usuariofechacreacion;
	private Timestamp usuariofechamodificacion;
	private Timestamp usuarioultimoacceso;
	private String usuariokey;
	private Perfil perfil;
	public int getUsuarioid() {
		return usuarioid;
	}
	public void setUsuarioid(int usuarioid) {
		this.usuarioid = usuarioid;
	}
	public int getPerfilid() {
		return perfilid;
	}
	public void setPerfilid(int perfilid) {
		this.perfilid = perfilid;
	}
	public int getEmpleadoid() {
		return empleadoid;
	}
	public void setEmpleadoid(int empleadoid) {
		this.empleadoid = empleadoid;
	}
	public boolean isUsuarioactivo() {
		return usuarioactivo;
	}
	public void setUsuarioactivo(boolean usuarioactivo) {
		this.usuarioactivo = usuarioactivo;
	}
	public String getUsuarionombre() {
		return usuarionombre;
	}
	public void setUsuarionombre(String usuarionombre) {
		this.usuarionombre = usuarionombre;
	}
	public String getUsuariousr() {
		return usuariousr;
	}
	public void setUsuariousr(String usuariousr) {
		this.usuariousr = usuariousr;
	}
	public String getUsuariopwd() {
		return usuariopwd;
	}
	public void setUsuariopwd(String usuariopwd) {
		this.usuariopwd = usuariopwd;
	}
	public Timestamp getUsuariofechacreacion() {
		return usuariofechacreacion;
	}
	public void setUsuariofechacreacion(Timestamp usuariofechacreacion) {
		this.usuariofechacreacion = usuariofechacreacion;
	}
	public Timestamp getUsuariofechamodificacion() {
		return usuariofechamodificacion;
	}
	public void setUsuariofechamodificacion(Timestamp usuariofechamodificacion) {
		this.usuariofechamodificacion = usuariofechamodificacion;
	}
	public Timestamp getUsuarioultimoacceso() {
		return usuarioultimoacceso;
	}
	public void setUsuarioultimoacceso(Timestamp usuarioultimoacceso) {
		this.usuarioultimoacceso = usuarioultimoacceso;
	}
	public String getUsuariokey() {
		return usuariokey;
	}
	public void setUsuariokey(String usuariokey) {
		this.usuariokey = usuariokey;
	}
	public Perfil getPerfil() {
		return perfil;
	}
	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}
	@Override
	public String toString() {
		return "Usuarios [usuarioid=" + usuarioid + ", perfilid=" + perfilid + ", empleadoid=" + empleadoid
				+ ", usuarioactivo=" + usuarioactivo + ", usuarionombre=" + usuarionombre + ", usuariousr=" + usuariousr
				+ ", usuariopwd=" + usuariopwd + ", usuariofechacreacion=" + usuariofechacreacion
				+ ", usuariofechamodificacion=" + usuariofechamodificacion + ", usuarioultimoacceso="
				+ usuarioultimoacceso + ", usuariokey=" + usuariokey + ", perfil=" + perfil + "]";
	}
}
