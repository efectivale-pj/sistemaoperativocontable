package com.efectivale.soc.utils;

public class Scripts {

	public static final String SPUpdateBitacora = "select * from bitacora_online_update(?, ?, ?, ?, ?, ?, ?, ?, ?)";
	public static final String SPcreateBitacora = "select * from bitacora_online_create(?, ?, ?, ?, ?, ?, ?, ?);";
	public static final String SPUpdateUsuario = "select * from usuarios_online_update(?,  ?,  ?,  ?,  ?,  ?,  ?,  ?,  ?,  ?);";
	public static final String SPCambiarContrasenia = "select * from usuarios_online_cambia_contrasenia(?,?,?);";
	public static final String SPvalidateUsuario = "select * from usuarios_online_validate_session(?,?,?)";
	public static final String ViewUsuarioById = "select * from view_usuario_validado where usuarioid=?";

}
