package com.efectivale.soc.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.efectivale.soc.dao.BitacoraDao;
import com.efectivale.soc.dao.impl.BitacoraDaoImpl;
import com.efectivale.soc.model.Bitacora;
import com.efectivale.soc.model.Perfil;
import com.efectivale.soc.model.Permisos;
import com.efectivale.soc.model.Procesos;
import com.efectivale.soc.model.Usuarios;

public class Utils {
	private Logger log = Logger.getLogger(Utils.class);
	private String DetailError;

	public boolean sessionActive(HttpServletRequest request) {
		Usuarios usuario = (Usuarios) request.getSession().getAttribute("usuariosession");
		
		if((usuario==null || usuario.getUsuarioid()<1) 
				|| (usuario.getPerfil().getPermisos()==null 
					|| usuario.getPerfil().getPermisos().size()<1))
		{
			DetailError="Tu session ha expirado, por favor inicia sesion nuevamente.";
			return false;
		}
		return true;
	}

	public Bitacora isSessionActive(HttpServletRequest request) {
		Usuarios usuario = (Usuarios) request.getSession().getAttribute("usuariosession");
		
		if((usuario==null || usuario.getUsuarioid()<1) 
				|| (usuario.getPerfil().getPermisos()==null || usuario.getPerfil().getPermisos().size()<1))
		{
			DetailError="Tu session ha expirado, por favor inicia sesion nuevamente.";
			return null;
		}
		return validaPermiso(request, usuario);
	}
	
	private Bitacora validaPermiso(HttpServletRequest request, Usuarios usuario) {
		BitacoraDao bitacoraDao = new BitacoraDaoImpl();
		Bitacora bitacora = new Bitacora();
		
		for(Permisos permiso: usuario.getPerfil().getPermisos())
		{
			if(request.getRequestURI().equals(permiso.getProcesos().getProcesopath()))
			{
				bitacora.setPermiso(permiso);
				break;
			}
		}
		if(bitacora.getPermiso()==null)
		{
			log.error("No cuentas con permisos para ingresar a esta opcion:: "+request.getRequestURI());
			DetailError="No cuentas con permisos para ingresar a esta opcion";
			return null;
		}
		bitacora.setProcesoid(bitacora.getPermiso().getProcesoid());
		bitacora.setUsuarioid(usuario.getUsuarioid());
		bitacora.setBitacoraerror("");
		bitacora.setBitacoramensaje("");
		bitacora.setBitacorapeticion("");
		bitacora.setBitacorarespuesta("");
		bitacora.setBitacoraip(request.getRemoteAddr());
		bitacora.setBitacorafecha(new Timestamp(System.currentTimeMillis()));
		if(!bitacoraDao.createBitacora(bitacora))
		{
			DetailError = bitacoraDao.getDetailError();
			return null;
		}
		return bitacora;
	}

	public void updateBitacora(Bitacora bitacora) {
		BitacoraDao bitacoraDao = new BitacoraDaoImpl();
		if(!bitacoraDao.updateBitacora(bitacora))
			log.error("No se logro actualizar la bitacora");
	}

	public Connection getConnection(String database) throws Exception
	{
		Connection myConn=null;
		try {
			InitialContext ctx = new InitialContext();
			Context env = (Context) ctx.lookup("java:comp/env");
			DataSource ds = (DataSource) env.lookup("jdbc/"+database);
			myConn=ds.getConnection();
		}catch (Exception ex) {
			ex.printStackTrace();
			log.fatal(ex);
			throw new Exception("Se genero un error al solicitar la conexion a la base de datos ["+database+"]. "+ex.getMessage());
		}
		finally
		{
			if(myConn==null)
				throw new Exception("No se logro conectar con la base de datos ["+database+"]");
		}
		return myConn;
	}

	public void closeConnection(Connection connection, PreparedStatement preparedStatement, ResultSet resultSet) 
	{
		try
		{
			if( resultSet != null )
				resultSet.close();
			
			if( preparedStatement != null)
				preparedStatement.close();
			
			if(connection!=null && !connection.isClosed())
				connection.close();
		}catch(Exception sql)
		{
			log.fatal(sql);
		}		
	}

	public String getDetailError() {
		return DetailError;
	}

	public Usuarios getObjectUsuario(ResultSet resultSet) throws Exception {
		Usuarios usuario;
		try {
			usuario = new Usuarios();
			usuario.setUsuarioid(resultSet.getInt("usuarioid"));
			usuario.setPerfilid(resultSet.getInt("perfilid"));
			usuario.setEmpleadoid(resultSet.getInt("empleadoid"));
			usuario.setUsuarioactivo(resultSet.getBoolean("usuarioactivo"));
			usuario.setUsuariousr(resultSet.getString("usuariousr"));
			usuario.setUsuariopwd(resultSet.getString("usuariopwd"));
			usuario.setUsuarionombre(resultSet.getString("usuarionombre"));
			usuario.setUsuariofechacreacion(resultSet.getTimestamp("usuariofechacreacion"));
			usuario.setUsuariofechamodificacion(resultSet.getTimestamp("usuariofechamodificacion"));
			usuario.setUsuarioultimoacceso(resultSet.getTimestamp("usuarioultimoacceso"));
			usuario.setUsuariokey(resultSet.getString("usuariokey"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Usuario:: "+ex.getMessage());
		}
		return usuario;
	}

	public Perfil getObjectPerfil(ResultSet resultSet) throws Exception{
		Perfil perfil;
		try {
			perfil = new Perfil();
			perfil.setPerfilid(resultSet.getInt("perfilid"));
			perfil.setPerfilactivo(resultSet.getBoolean("perfilactivo"));
			perfil.setPerfilnombre(resultSet.getString("perfilnombre"));
			perfil.setPerfildescripcion(resultSet.getString("perfildescripcion"));
			perfil.setPerfilfechacreacion(resultSet.getTimestamp("perfilfechacreacion"));
			perfil.setPerfilfechamodificacion(resultSet.getTimestamp("perfilfechamodificacion"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Perfil:: "+ex.getMessage());
		}
		return perfil;
	}

	public Permisos getObjectPermisos(ResultSet resultSet) throws Exception{
		Permisos permiso;
		try {
			permiso = new Permisos();
			permiso.setPermisoid(resultSet.getInt("permisoid"));
			permiso.setPermisoactivo(resultSet.getBoolean("permisoactivo"));
			permiso.setPerfilid(resultSet.getInt("perfilid"));
			permiso.setProcesoid(resultSet.getInt("procesoid"));
			permiso.setPermisofechacreacion(resultSet.getTimestamp("permisofechacreacion"));
			permiso.setPermisofechamodificacion(resultSet.getTimestamp("permisofechamodificacion"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Permisos:: "+ex.getMessage());
		}
		return permiso;
	}

	public Procesos getObjectProcesos(ResultSet resultSet) throws Exception{
		Procesos proceso;
		try {
			proceso = new Procesos();
			proceso.setProcesoid(resultSet.getInt("procesoid"));
			proceso.setProcesoactivo(resultSet.getBoolean("procesoactivo"));
			proceso.setProcesonombre(resultSet.getString("procesonombre"));
			proceso.setProcesodescripcion(resultSet.getString("procesodescripcion"));
			proceso.setProcesofechacreacion(resultSet.getTimestamp("procesofechacreacion"));
			proceso.setProcesofechamodificacion(resultSet.getTimestamp("procesofechamodificacion"));
			proceso.setProcesopath(resultSet.getString("procesopath"));
			proceso.setProcesopadreid(resultSet.getInt("procesopadreid"));
			proceso.setProcesoprincipal(resultSet.getBoolean("procesoprincipal"));
			proceso.setProcesoorden(resultSet.getInt("procesoorden"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("No se logro crear el objeto Procesos:: "+ex.getMessage());
		}
		return proceso;
	}

}
